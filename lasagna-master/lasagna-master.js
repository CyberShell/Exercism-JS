/// <reference path="./global.d.ts" />
// @ts-check

/**
 * Implement the functions needed to solve the exercise here.
 * Do not forget to export them so they are available for the
 * tests. Here an example of the syntax as reminder:
 *
 * export function yourFunction(...) {
 *   ...
 * }
 */

/**
 * Fuction that ouputs cooking status based on remaining time as input.
 * 
 * 
 * @param {number} remainingTime
 * @returns {string}
 */
export function cookingStatus(remainingTime)
{
    if (remainingTime === null || remainingTime === undefined) 
    {
        return 'You forgot to set the timer.';
    }
    else if (remainingTime == 0) 
    {
        return 'Lasagna is done.';
    } 
    else 
    {
        return 'Not done, please wait.';
    }
}

/**
 * Function that calculates the preparation time for the lasagna based on
 * the number of layers and the prep time per layer. 
 * The layers are an array, and a default of 2 is used for the prep time.
 * 
 * @param {string[]} layers
 * @param {number} prepTimePerLayer
 * @returns {number} 
 */
export function preparationTime(layers, prepTimePerLayer = 2)
{
    return layers.length * prepTimePerLayer;
}

/**
 * Function calculates the amount of noodles and sauce needed
 * Input: array of layers
 * Output: object with keys noodles and sauce
 * 
 * @param {string[]} layers
 * @returns {Record<number, number>}
 */
export function quantities(layers)
{
    // sauce is measured in liters, and noodles is measured in grams
    let sauceNeeded = 0, noodlesNeeded = 0;
    for (let layer = 0; layer < layers.length; layer++) {
        if (layers[layer] === 'noodles')
        {
            noodlesNeeded += 50;
        }
        else if (layers[layer] === 'sauce')
        {
            sauceNeeded += 0.2;
        }
    }
    const quantitieOfIngredients = {
        'noodles': noodlesNeeded,
        'sauce': sauceNeeded,
    }
    return quantitieOfIngredients;
}

/**
 * 
 * @param {string[]} friendsList ingredients provided by friend
 * @param {string[]} myList my list of ingredients
 */
export function addSecretIngredient(friendsList, myList)
{
    var itemToBeAdded = friendsList[friendsList.length - 1];
    myList[myList.length] = itemToBeAdded;
}

/**
 * 
 * @param {Object} originalRecipe contains amounts for two portions
 * @param {number} portions 
 */
export function scaleRecipe(originalRecipe, portions)
{
    var newRecipe = {};
    for (var ingredient in originalRecipe) {
        // divide original amount by 2
        var amountForOne = originalRecipe[ingredient]/2;
        newRecipe[ingredient] = amountForOne * portions;
    }
    return newRecipe;
}
// @ts-check
//
// The line above enables type checking for this file. Various IDEs interpret
// the @ts-check directive. It will give you helpful autocompletion when
// implementing this exercise.

/**
 * Calculates the total bird count.
 *
 * @param {number[]} birdsPerDay
 * @returns {number} total bird count
 */
export function totalBirdCount(birdsPerDay) {
  var totalNumBirds = 0, total = 0;
  for (let i = 0; i < birdsPerDay.length; i++) {
    totalNumBirds = birdsPerDay[i];
    total += totalNumBirds;
  }
  // alternative way using iterators

  // birdsPerDay.forEach(element => {
  //    totalNumBirds += element});
  return total;
}

/**
 * Calculates the total number of birds seen in a specific week.
 *
 * @param {number[]} birdsPerDay
 * @param {number} week
 * @returns {number} birds counted in the given week
 */
export function birdsInWeek(birdsPerDay, week) {
  var totalNumBirdsWeek = 0, total = 0;
  var weekStart = 7*week-7;
  for (let i = weekStart; i < 7*week; i++) {
    totalNumBirdsWeek += birdsPerDay[i];
  }
  return totalNumBirdsWeek;
}

/**
 * Fixes the counting mistake by increasing the bird count
 * by one for every second day.
 *
 * @param {number[]} birdsPerDay
 * @returns {number[]} corrected bird count data
 */
export function fixBirdCountLog(birdsPerDay) {
  // using for-loop
  for (let i = 0; i < birdsPerDay.length; i+=2) {
    birdsPerDay[i]++;
  }
  return birdsPerDay;
}

// @ts-check

/**
 * Implement the classes etc. that are needed to solve the
 * exercise in this file. Do not forget to export the entities
 * you defined so they are available for the tests.
 */

export function Size(width = 80, height = 60)
{
    this.width = width;
    this.height = height;

    
}

Size.prototype.resize = function (newWidth, newHeight) {
    this.width = newWidth;
    this.height = newHeight;
};

export function Position(x = 0, y = 0)
{
    this.x = x;
    this.y = y;
}

Position.prototype.move = function (newX, newY)
{
    this.x = newX;
    this.y = newY;
}

export class ProgramWindow {
    constructor(){
        this.screenSize = new Size(800, 600);
        this.size = new Size();
        this.position = new Position();
    }

    resize(Size)
    {
        if (Size.width < 1) {
            this.size.width = 1;   
        }
        if (Size.height < 1)
        {
            this.size.height = 1;
        }
        if (this.screenSize.width < Size.width) {
            this.size.width = this.screenSize.width;
        }
        if (this.screenSize.width < Size.width) {
            this.size.width = this.screenSize.width;
        }
        if(this.size.height != 1)
        {
            this.size.height = Size.height;
        }
        if(this.size.width != 1)
        {
            this.size.width = Size.width;
        }
    }

    move(position)
    {
        if(position.x>this.screenSize.width-this.size.width){
            position.x=this.screenSize.width-this.size.width;
          }
          if(position.y>this.screenSize.height-this.size.height){
            position.y=this.screenSize.height-this.size.height;
          }
          position.x=position.x>=0?position.x:0;
          position.y=position.y>=0?position.y:0;
          this.position=position;
    }
}

export function changeWindow(ProgramWindow)
{
    ProgramWindow.resize(new Size(400, 300));
    ProgramWindow.move(new Position(100, 150));

    return ProgramWindow;
}
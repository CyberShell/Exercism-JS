// @ts-check

/**
 * Calculates the sum of the two input arrays.
 *
 * @param {number[]} array1
 * @param {number[]} array2
 * @returns {number} sum of the two arrays
 */
export function twoSum(array1, array2) {
  var num1 = Number(String(array1.join('')));
  var num2 = Number(String(array2.join('')));
  return num1 + num2;
}

/**
 * Checks whether a number is a palindrome.
 *
 * @param {number} value
 * @returns {boolean}  whether the number is a palindrome or not
 */
export function luckyNumber(value) {
  var firstString = String(value);
  var splitString = firstString.split('');
  var reversedString = splitString.reverse();
  var reversedNumber = Number(reversedString.join(''));
  if (reversedNumber == value) {
    return true;
  }
  return false;
}

/**
 * Determines the error message that should be shown to the user
 * for the given input value.
 *
 * @param {string|null|undefined} input
 * @returns {string} error message
 */
export function errorMessage(input) {
  if (input === undefined || input === null || input == '')
  {
    return 'Required field';
  }
  else if (!Number(input) || Number(input) == 0) 
  {
    return 'Must be a number besides 0';
  }
  else
  {
    return '';
  }
}
